#Bilibili客户端下载视频自动合并

####总览
本项目基于java，测试哔哩哔哩手机客户端版本为5.0.0，兼容以前版本下载的视频。
伸手党可以直接[点击这里下载](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/BilibiliMeroV1.2.7z)压缩包解压后双击 开始运行.bat 即可
已安装JAVA运行环境可[点击直接下载jar包](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/bilibili_mergeV1.2.jar)
```windows+R-->CMD-->java -jar C:\Users\Administrator\Desktop\bilibili_mergeV1.2.jar``` 即可

####实现功能
- 1.自动识别文件夹下视频文件并进行合并
- 2.合并后以视频播放页视频名称+视频分P名称命名,如 ```F:\(日剧)夺爱之冬\第一话.flv```
- 3.合并完成删除源文件

实现过程请移步至我的简书[http://www.jianshu.com/p/73dadc6a5829](http://www.jianshu.com/p/73dadc6a5829)

####更新日志
- V1.2  
可一次输入多个哔哩哔哩视频目录地址，以英文逗号隔开  
更改源文件删除逻辑  
修复视频合并顺序的问题  
修复下载的视频不是从第一P下载而出现不能合并的问题  
修复下载番剧不能合并问题  
分P文件命名增加索引以显示正确的分P排序(和哔哩哔哩显示的分P排序一致)  
修复文件名包含不允许命名规则的字符产生的合并失败问题  
- V1.1  
解决在win上乱码的问题
- V1.0  
完成基本功能

####使用方法
- 1.将哔哩哔哩手机客户端下载的视频移出手机的Android目录，如移动到根目录
因android MTP限制，电脑无法访问Android目录。此目录是Android应用缓存目录。
视频位于 ```Android--data--tv.danmaku.bili(最下面)--download```下。如图显示的数字目录即为需求目录。请将数字目录移出Android目录
![](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/Screenshot_02.png)

- 2.手机连上电脑后，将上述数字目录复制或移动到电脑

- 3.电脑找到数字目录，复制其全路径，如图
![](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/Screenshot_03.png)

- 4.解压运行
解压[下载的文件](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/BilibiliMeroV1.2.7z),双击 **开始运行.bat** 。
出现输入目录的提示后，右键粘贴刚才复制的目录，若目录不存在会要求重新输入。
可一次输入多个哔哩哔哩视频目录地址，以英文逗号隔开。
![](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/Screenshot_04.png)
**请不要输入错误的目录或只输入某个盘符**，如 ```C:```因为最后会删除这个目录，因此造成的不可估量的后果请您自负！！
然后输入合并后的输出目录，如```F:```。输出文件夹不能和输入文件夹一样，原因同上。
完成后会自动运行。

- 5.程序会自动合并，合并完成后会删除原文件夹，程序会自动退出。

####Q&A  
>Q1:下载的压缩文件我为什么打不开？
A1:因JRE过大，上传压缩文件采用最大压缩率的7z文件。请下载解压缩软件再试。推荐下载[360压缩 http://yasuo.360.cn/](http://yasuo.360.cn/)。

>Q2:我以前下载的视频已经合并了，但是进入的目录层级太多，而且不能显示正确的分P名(如图)。我可以使用该工具重命名吗？
![](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/Screenshot_05.png)
A2:可以，只要保证图中的 ```entry.json``` 文件没有被删除以及上图中文件夹 ```lua.flv...``` 名称没有被修改即可，不会有其他影响。如图是使用本工具重命名后的结果。
![](http://git.oschina.net/chengww5217/BiliBiliMerge/raw/master/run/Screenshot_06.png)　

>Q3:上述问题中文件名为什么会有两个01？
A3:出现这种情况是因为视频分P名已经包含索引，但是绝大多数的分P名是不包含索引的，所以少数情况会出现这种问题，请放心。

>Q4:为什么有的视频合并后会缺失某一段时间的内容？
A4:有的视频没有完全下载完毕，如后缀为```.blv.bdl```的文件，程序也会强行对其进行合并(有时也会因读取不到flv tag而跳过)。
这并不是程序的问题，你应当检查文件是否下载或写入完毕，在进行合并。

>Q5:为什么合并后的文件夹为空？
A5:检查你下载文件的文件夹是否有```.blv```或```.flv```文件。如果确定是程序的问题，欢迎反馈issue或提交问题到我的邮箱：
chengww5217@163.com


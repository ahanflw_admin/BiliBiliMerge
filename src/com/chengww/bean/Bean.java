package com.chengww.bean;
public class Bean {

	private int avid;
    private String cover;
    private int downloaded_bytes;
    private int guessed_total_bytes;
    private boolean is_completed;
    private PageDataBean page_data;
    private EpBean ep;
    private int prefered_video_quality;
    private int seasion_id;
    private int spid;
    private String title;
    private int total_bytes;
    private int total_time_milli;
    private String type_tag;

    public int getAvid() {
        return avid;
    }

    public void setAvid(int avid) {
        this.avid = avid;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public int getDownloaded_bytes() {
        return downloaded_bytes;
    }

    public void setDownloaded_bytes(int downloaded_bytes) {
        this.downloaded_bytes = downloaded_bytes;
    }

    public int getGuessed_total_bytes() {
        return guessed_total_bytes;
    }

    public void setGuessed_total_bytes(int guessed_total_bytes) {
        this.guessed_total_bytes = guessed_total_bytes;
    }

    public boolean isIs_completed() {
        return is_completed;
    }

    public void setIs_completed(boolean is_completed) {
        this.is_completed = is_completed;
    }

    public PageDataBean getPage_data() {
        return page_data;
    }

    public void setPage_data(PageDataBean page_data) {
        this.page_data = page_data;
    }

    public int getPrefered_video_quality() {
        return prefered_video_quality;
    }

    public void setPrefered_video_quality(int prefered_video_quality) {
        this.prefered_video_quality = prefered_video_quality;
    }

    public int getSeasion_id() {
        return seasion_id;
    }

    public void setSeasion_id(int seasion_id) {
        this.seasion_id = seasion_id;
    }

    public int getSpid() {
        return spid;
    }

    public void setSpid(int spid) {
        this.spid = spid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getTotal_bytes() {
        return total_bytes;
    }

    public void setTotal_bytes(int total_bytes) {
        this.total_bytes = total_bytes;
    }

    public int getTotal_time_milli() {
        return total_time_milli;
    }

    public void setTotal_time_milli(int total_time_milli) {
        this.total_time_milli = total_time_milli;
    }

    public String getType_tag() {
        return type_tag;
    }

    public void setType_tag(String type_tag) {
        this.type_tag = type_tag;
    }

    public static class PageDataBean {
        /**
         * cid : 6850643
         * from : vupload
         * has_alias : false
         * link : 
         * page : 1
         * part : 第1话
         * raw_vid : 
         * rich_vid : 
         * tid : 0
         * type : vupload
         * vid : vupload_6850643
         * weblink : 
         */

        private int cid;
        private String from;
        private boolean has_alias;
        private String link;
        private int page;
        private String part;
        private String raw_vid;
        private String rich_vid;
        private int tid;
        private String type;
        private String vid;
        private String weblink;

        public int getCid() {
            return cid;
        }

        public void setCid(int cid) {
            this.cid = cid;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public boolean isHas_alias() {
            return has_alias;
        }

        public void setHas_alias(boolean has_alias) {
            this.has_alias = has_alias;
        }

        public String getLink() {
            return link;
        }

        public void setLink(String link) {
            this.link = link;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public String getPart() {
            return part;
        }

        public void setPart(String part) {
            this.part = part;
        }

        public String getRaw_vid() {
            return raw_vid;
        }

        public void setRaw_vid(String raw_vid) {
            this.raw_vid = raw_vid;
        }

        public String getRich_vid() {
            return rich_vid;
        }

        public void setRich_vid(String rich_vid) {
            this.rich_vid = rich_vid;
        }

        public int getTid() {
            return tid;
        }

        public void setTid(int tid) {
            this.tid = tid;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getVid() {
            return vid;
        }

        public void setVid(String vid) {
            this.vid = vid;
        }

        public String getWeblink() {
            return weblink;
        }

        public void setWeblink(String weblink) {
            this.weblink = weblink;
        }
    }
    
    public EpBean getEp() {
        return ep;
    }

    public void setEp(EpBean ep) {
        this.ep = ep;
    }
    
    public static class EpBean {
        /**
         * av_id : 4922937
         * cover : http://i0.hdslb.com/bfs/archive/d45440607b3aa35a6fadd459a6e90336d70edbee.jpg
         * danmaku : 10732879
         * episode_id : 89139
         * episode_status : 0
         * index : 9
         * index_title : 坂本君与我的相遇
         * page : 0
         */

        private int av_id;
        private String cover;
        private int danmaku;
        private int episode_id;
        private int episode_status;
        private String index;
        private String index_title;
        private int page;

        public int getAv_id() {
            return av_id;
        }

        public void setAv_id(int av_id) {
            this.av_id = av_id;
        }

        public String getCover() {
            return cover;
        }

        public void setCover(String cover) {
            this.cover = cover;
        }

        public int getDanmaku() {
            return danmaku;
        }

        public void setDanmaku(int danmaku) {
            this.danmaku = danmaku;
        }

        public int getEpisode_id() {
            return episode_id;
        }

        public void setEpisode_id(int episode_id) {
            this.episode_id = episode_id;
        }

        public int getEpisode_status() {
            return episode_status;
        }

        public void setEpisode_status(int episode_status) {
            this.episode_status = episode_status;
        }

        public String getIndex() {
            return index;
        }

        public void setIndex(String index) {
            this.index = index;
        }

        public String getIndex_title() {
            return index_title;
        }

        public void setIndex_title(String index_title) {
            this.index_title = index_title;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }
    }
}